package com.base.java.model;

import lombok.*;
import org.springframework.data.geo.Point;

@Data
@Builder(access = AccessLevel.PUBLIC)
@NoArgsConstructor
@AllArgsConstructor
public class Location {
    private String id;
    private Point geoLocation;
    private String name;
}
