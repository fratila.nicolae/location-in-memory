package com.base.java;

import com.base.java.model.Circle;
import com.base.java.model.Location;
import lombok.AllArgsConstructor;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.geo.Point;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

@SpringBootApplication
@RestController
@AllArgsConstructor
public class JavaApplication {

    public static void main(String[] args) {
        SpringApplication.run(JavaApplication.class, args);
    }

    private List<Location> locations;

    @PostConstruct
    public void generateData() {
        int i = 0;
        Random random = new Random(69);
        while (i < 100000) {
            Location location = Location.builder()
                    .id("" + i)
                    .geoLocation(new Point(random.nextDouble() * 360 - 180, random.nextDouble() * 90 - 45))
                    .name("random name" + i).build();
            locations.add(location);
            i++;
        }
    }

    @PostMapping("fetch")
    public List<Location> fetchData(@RequestBody Circle circle) {
        return locations.stream()
                .filter(i -> distance(i.getGeoLocation(), circle.getPoint()) < circle.getDistance())
                .collect(Collectors.toList());
    }

    @GetMapping("fetch-all")
    public List<Location> fetchAllData() {
        return locations;
    }

    public static double distance(Point p1, Point p2) {

        final int R = 6371; // Radius of the earth

        double latDistance = Math.toRadians(p2.getX() - p1.getX());
        double lonDistance = Math.toRadians(p2.getY() - p2.getY());
        double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2)
                + Math.cos(Math.toRadians(p1.getX())) * Math.cos(Math.toRadians(p2.getX()))
                * Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

        return R * c * 1000; // convert to meters
    }
}
